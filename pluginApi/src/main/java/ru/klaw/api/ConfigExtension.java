package ru.klaw.api;

import org.yaml.snakeyaml.TypeDescription;

import java.util.List;

public interface ConfigExtension {
    List<TypeDescription> getTypeDescriptions();
}
