package ru.klaw.plugin;

import org.yaml.snakeyaml.TypeDescription;
import ru.klaw.api.ConfigExtension;

import java.util.Collections;
import java.util.List;

public class SiebelURLConfig implements ConfigExtension {
    final List<TypeDescription> typeDescriptions;

    public SiebelURLConfig() {
        typeDescriptions= Collections.singletonList(new TypeDescription(SiebelURL.class, "!siebelURL"));
    }

    @Override
    public List<TypeDescription> getTypeDescriptions() {
        return typeDescriptions;
    }
}
