package ru.klaw;

import ru.klaw.api.ConfigExtension;

import java.util.List;

public interface ConfigProvider {
    /**
     * @return Возвращает коллекцию ConfigExtension
     */
    List<ConfigExtension> getConfigExtensions();

    /**
     * Обновляет возращаемый список конфигураций
     */
    void reload();
}
