package ru.klaw.web;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import ru.klaw.ConfigServiceLoader;
import ru.klaw.FileReader;
import ru.klaw.ParserService;
import ru.klaw.plugin.AttachmentPath;
import ru.klaw.plugin.SiebelURL;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;


class YamlParserApplicationTests {

    static Path path;

    @BeforeAll
    static void beforeAll(@TempDir Path tempDir) throws IOException {
        path = tempDir.resolve("config.yaml");

        final InputStream resource = Objects.requireNonNull(YamlParserApplicationTests.class.getClassLoader().getResourceAsStream("config.yaml"));
        //Сохраняем yaml файл из resources во временную папку
        Files.copy(resource,path, StandardCopyOption.REPLACE_EXISTING);
    }

    @Test
    void testParse() {
        final String yamlString = FileReader.read(path.toString());
        ParserService parser = new ParserService(new ConfigServiceLoader());

        final List<Object> objectList = parser.parse(yamlString);

        System.out.println(objectList);
        Object object = objectList.get(0);
        assertEquals(SiebelURL.class, object.getClass());
        SiebelURL siebelURL = (SiebelURL) object;
        assertEquals(siebelURL.getEndpoint(), "endpoint1");
        assertEquals(siebelURL.getLogin(), "login1");
        assertEquals(siebelURL.getPassword(), "password1");

        object = objectList.get(1);
        assertEquals(AttachmentPath.class, object.getClass());
        AttachmentPath attachmentPath = (AttachmentPath) object;
        assertEquals(attachmentPath.getLocalRootDir(), "localRootDir1");
        assertEquals(attachmentPath.getLocalUploadDir(), "localUploadDir1");
        assertEquals(attachmentPath.getRemoteRootDir(), "remoteRootDir1");

        object = objectList.get(2);
        assertEquals(SiebelURL.class, object.getClass());
        SiebelURL siebelURL2 = (SiebelURL) object;
        assertEquals(siebelURL2.getEndpoint(), "endpoint2");
        assertEquals(siebelURL2.getLogin(), "login2");
        assertEquals(siebelURL2.getPassword(), "password2");

    }

}
