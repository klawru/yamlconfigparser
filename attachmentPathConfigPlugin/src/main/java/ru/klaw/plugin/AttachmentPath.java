package ru.klaw.plugin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AttachmentPath {
    String localRootDir;
    String localUploadDir;
    String remoteRootDir;
}
